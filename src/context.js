import React, { Component } from 'react';
import {storeProducts, detailProduct} from './data';
import ProductList from './componets/ProductList';


const ProductContext = React.createContext();
//provider
//consumer
class ProductProvider extends Component {
    
    
      state = {
          product:[],
          detailProduct:detailProduct,
          cart:[],
          cartSubtotal:0,
          cartTax:0,
          cartTotal:0
      };
      
      componentDidMount(){
        this.setProducts();

    }
   

       
      setProducts=()=>{
          let tempProducts = [];
          storeProducts.forEach(item=>{
              const singleItem ={...item};
              tempProducts = [...tempProducts,singleItem]
          })
          this.setState(()=>{
           return {product:tempProducts};
          })
      }

      getItem = (id)=>{
        const product = this.state.product.find(item=>item.id===id);
        return product;
      }
      
      handleDetail = (id) =>{
          const product = this.getItem(id);
          this.setState(()=>{
            return {detailProduct:product};
          })
      }

      addToCart = (id) =>{
        let tempProducts = [...this.state.product];
        const index = tempProducts.indexOf(this.getItem(id))
        ;
        const product = tempProducts[index];
        product.inCart = true;
        product.count = 1;
        const price = product.price;
        product.total = price;
        this.setState(
          ()=>{
          return {products:tempProducts,cart:[...this.state.cart,
            product] };
          },
          ()=>{
            this.addTotals();
          }
        )

      }



      increment = (id)=>{
        console.log('this is increment method');
        let tempCart = [...this.state.cart];
        const selectedProduct =tempCart.find(item=>item.id === id)
        const index = tempCart.indexOf(selectedProduct);
        const product  = tempCart[index];
        product.count = product.count + 1;
        product.total = product.count * product.price;

        this.setState(()=>{
          return {cart:[...tempCart]}

        },()=>{
          this.addTotals();
        })

      }
      
      decrement = (id)=>{
        console.log('this is dencrement method');
        let tempCart = [...this.state.cart];
        const selectedProduct =tempCart.find(item=>item.id === id)
        const index = tempCart.indexOf(selectedProduct);
        const product  = tempCart[index];
        product.count = product.count - 1;

        if(product.count == 0){
          console.log(product.count);
          this.removeItem(id);
        }

        else{
        product.total = product.count * product.price;
        }
        this.setState(()=>{
          return {cart:[...tempCart]}

        })

      }



      removeItem = (id)=>{
        console.log('item removed');
        let tempProducts = [...this.state.product]
        let tempCart = [...this.state.cart]

        tempCart = tempCart.filter(item =>item.id !=id);
        const index = tempProducts.indexOf(this.getItem(id));
        const  removedProduct = tempProducts[index];
        removedProduct.inCart = false;
        removedProduct.count = 0;
        removedProduct.total = 0;
        this.setState(()=>{
          return{
            cart:[...tempCart],
            product:[...tempProducts]

          }
        },()=>{
          this.addTotals();
        })

      }
       
      clearCart = ()=>{
        console.log('cart cleared');
        this.setState(()=>{
          return {cart:[]}
        },()=>{
          this.setProducts();
          this.addTotals();
        }
        )
      }

      addTotals = ()=>{
        let Subtotal = 0;
        this.state.cart.map(item=>(Subtotal += item.total))
        const tempTax = Subtotal*0.1;
        const tax = parseFloat(tempTax.toFixed(2));
        const total = Subtotal + tax;
        this.setState(()=>{
          return {
            cartSubtotal:Subtotal,
            cartTax:tax,
            cartTotal:total
          }
          
        })
      }

    render() { 

        return (<ProductContext.Provider value={{...this.state,
                                                 handleDetail:this.handleDetail,
                                                 addToCart:this.addToCart,
                                                 increment:this.increment,
                                                 decrement:this.decrement,
                                                 removeItem:this.removeItem,
                                                 clearCart:this.clearCart,
                                                 }}>
           {this.props.children}        
        </ProductContext.Provider>
         
         )
    }
}

const ProductConsumer = ProductContext.Consumer;

export {ProductProvider,ProductConsumer};
 
