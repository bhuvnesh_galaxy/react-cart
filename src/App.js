import React from 'react';
import {Switch,Route } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import '@fortawesome/fontawesome-free/css/all.css'
import Navbar from './componets/Navbar'
import ProductList from './componets/ProductList'
import Details from './componets/Details'
import Cart from './componets/cart/Cart'
import Default from './componets/Default'
import Store from './componets/Store'




function App() {
  return (
    <React.Fragment>
      <Navbar></Navbar>
     <Switch>
     <Route path="/" exact component = {ProductList}></Route>
     <Route path="/details" component = {Details}></Route>
     <Route path="/cart" component = {Cart}></Route>
     <Route component={Default} />
     </Switch>
   
    </React.Fragment>
  );
}

export default App;
